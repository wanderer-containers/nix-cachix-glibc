# nix-cachix-glibc
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/nix-cachix-glibc/status.svg)](https://drone.dotya.ml/wanderer-containers/nix-cachix-glibc)

the Containerfile in this repo simply installs `cachix` and `glibc` packages on
top of the base image, the rationale being this image can be cached for reuse
in CI (such as [Drone](https://drone.io) with ephemeral containers).  
based on `docker.io/nixos/nix:2.8.0-amd64`, weekly rebuilt on cron.

### LICENSE
GPL-3.0
